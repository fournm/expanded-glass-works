# README #

Adds a bunch of green glass, clear glass, and crystal glass items to the game for Dwarven civs. Bins, beds, and short swords. PRs welcome, if this breaks.

* v3 -- 05/06/2020 -- Updated to 47.x compatibility
* v2 -- 11/29/2017 -- Updated to 44.x compatibility
* v1 -- Initial release

### Compatibility ###

* DF 47.x